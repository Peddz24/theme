<?php get_header();

global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

foreach($query_args as $key => $string) :
    $query_split = explode("=", $string);
    $search_query[$query_split[0]] = urldecode($query_split[1]);
endforeach;

$search = new WP_Query($search_query);
?>

<section class="pagewrap">
    <article id="page_content">
<h1 class="title" class="">Zoeken</h1>

<?php
get_search_form();

if ($search_query['s'] != '' && $search->have_posts()) {
    while ( $search->have_posts() ) : $search->the_post(); ?>

        <?php get_template_part('partials/content', 'listitem') ?>

    <?php endwhile;
?>

<section class="pagination">
    <?php echo paginate_links(  ); ?>
</section>

<?php } else {
    echo 'Er zijn helaas geen resultaten gevonden';
}?>

    </article>
</section>

<?php get_footer(); ?>
