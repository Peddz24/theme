<?php

namespace coderehab\Composer;

use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;

class ThemeInstaller extends LibraryInstaller{

    public function getInstallPath( PackageInterface $package ) {

        $installationDir = false;
		$prettyName      = $package->getPrettyName();
		if ( $this->composer->getPackage() ) {
			$topExtra = $this->composer->getPackage()->getExtra();
			if ( ! empty( $topExtra['coderehab-theme-dir'] ) ) {
				$installationDir = $topExtra['coderehab-theme-dir'];
				if ( is_array( $installationDir ) ) {
					$installationDir = empty( $installationDir[$prettyName] ) ? false : $installationDir[$prettyName];
				}
			}
		}
		$extra = $package->getExtra();
		if ( ! $installationDir && ! empty( $extra['coderehab-theme-dir'] ) ) {
			$installationDir = $extra['coderehab-theme-dir'];
		}
		if ( ! $installationDir ) {
			$installationDir = 'wp';
		}
		if (
			! empty( self::$_installedPaths[$installationDir] ) &&
			$prettyName !== self::$_installedPaths[$installationDir]
		) {
			throw new \InvalidArgumentException( 'Two packages cannot share the same directory!' );
		}
		self::$_installedPaths[$installationDir] = $prettyName;
		return $installationDir;

    }

    public function supports($packageType)
    {
        return 'coderehab-theme' === $packageType;
    }


}

