<?php get_header(); ?>
<section class="pagewrap">
    <article id="page_content">
        <?php get_template_part('partials/content', 'single') ?>
    </article>
</section>
<?php get_footer(); ?>
