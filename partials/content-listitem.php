<section class="post">
    <h2><a href="<?php the_permalink();?>"><?php echo get_the_title() ?></a></h2>
    <?php the_excerpt();?>
    <a href="<?php the_permalink();?>" class="btn">Lees verder <i class="fa fa-arrow-right"></i></a>
</section>
