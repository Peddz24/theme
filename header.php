<!doctype html>

<html>
    <head>
        <meta charset="utf-8">
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="author" content="Code.Rehab" />
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <!--<meta name="theme-color" content="#f57823" >-->

        <title><?php wp_title(); ?></title>

        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/site.css"  />
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

        <?php wp_head(); ?>
    </head>

    <body <?php body_class() ?> >

    <header id="main">
        <section class="pagewrap">
            <?php wp_nav_menu( array('theme_location' => 'main-menu') ); ?>
        </section>
    </header>
